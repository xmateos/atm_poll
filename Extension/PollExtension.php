<?php
namespace ATM\PollBundle\Extension;

use Doctrine\ORM\EntityManager;
use ATM\PollBundle\Services\SearchVotes;

class PollExtension extends \Twig_Extension{

    private $searchVotes;

    public function __construct(SearchVotes $searchVotes)
    {
        $this->searchVotes = $searchVotes;
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('ATMPollItemTotalVotes', array($this, 'itemTotalVotes')),
            new \Twig_SimpleFunction('ATMPollUserAlreadyVoted', array($this, 'userAlreadyVoted'))
        );
    }

    public function itemTotalVotes($itemId){
        $itemVotesCount = $this->searchVotes->search(array(
            'item_id' => $itemId,
            'count' => true
        ));

        return $itemVotesCount['count'];
    }

    public function userAlreadyVoted($poll,$userId){
        $itemVotesCount = $this->searchVotes->search(array(
                'user_id' => $userId,
                'poll_id' => $poll->getId(),
                'count' => true
            ));

        return $itemVotesCount['count'] < $poll->getMaxNumberVotes() ? false : true ;
    }
}