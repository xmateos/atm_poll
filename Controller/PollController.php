<?php

namespace ATM\PollBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use ATM\PollBundle\Services\SearchVotes;
use ATM\PollBundle\Entity\Poll;
use ATM\PollBundle\Form\PollType;
use ATM\PollBundle\Entity\Item;
use ATM\PollBundle\Document\Vote;


class PollController extends Controller
{
    /**
     * @Route("/poll/create", name="atm_poll_create")
     */
    public function createPollAction()
    {
        $poll = new Poll();
        $form = $this->createForm(PollType::class,$poll);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $config = $this->getParameter('atm_poll_config');
            if(!is_dir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'])){
                mkdir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder']);
            }

            $pollFolder = md5(uniqid());
            $pathToFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$pollFolder;
            mkdir($pathToFolder);

            $poll->setFolder($pollFolder);

            $em->persist($poll);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_poll_list'));
        }

        $em = $this->getDoctrine()->getManager();
        $forbiddenDates = $em->getRepository('ATMPollBundle:Poll')->getForbiddenDates();

        return $this->render('ATMPollBundle:Poll:create.html.twig',array(
            'form' => $form->createView(),
            'forbiddenDates' => json_encode($forbiddenDates)
        ));
    }

    /**
     * @Route("/poll/list/{page}", name="atm_poll_list", defaults={"page":1} )
     */
    public function listPollsAction($page){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('p')
            ->from('ATMPollBundle:Poll','p')
            ->orderBy('p.init_date','DESC');

        $pagination = $this->get('knp_paginator')->paginate(
            $qb->getQuery()->getArrayResult(),
            $page,
            10
        );

        return $this->render('ATMPollBundle:Poll:list.html.twig',array(
            'polls' => $pagination->getItems(),
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/edit/poll/{pollId}", name="atm_poll_edit")
     */
    public function editPollAction($pollId){
        $em = $this->getDoctrine()->getManager();

        $poll = $em->getRepository('ATMPollBundle:Poll')->findOneById($pollId);
        $form = $this->createForm(PollType::class,$poll);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($poll);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_poll_list'));
        }

        $forbiddenDates = $em->getRepository('ATMPollBundle:Poll')->getForbiddenDates($pollId);

        return $this->render('ATMPollBundle:Poll:edit.html.twig',array(
            'form' => $form->createView(),
            'poll' => $poll,
            'forbiddenDates' => json_encode($forbiddenDates)
        ));
    }

    /**
     * @Route("/delete/poll/{pollId}", name="atm_poll_delete", options={"expose"=true})
     */
    public function deletePollAction($pollId){
        $em = $this->getDoctrine()->getManager();
        $poll = $em->getRepository('ATMPollBundle:Poll')->findOneById($pollId);

        $config = $this->getParameter('atm_poll_config');

        $fileSystem = new Filesystem();
        $fileSystem->remove(array('folder',$this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$poll->getFolder()));

        $em->remove($poll);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_poll_list'));
    }

    /**
     * @Route("/add/poll/item/{pollId}", name="atm_poll_add_item")
     */
    public function addItemAction($pollId){
        $em = $this->getDoctrine()->getManager();
        $poll = $em->getRepository('ATMPollBundle:Poll')->findOneById($pollId);
        $request = $this->get('request_stack')->getCurrentRequest();

        if($request->getMethod() == 'POST'){

            $config = $this->getParameter('atm_poll_config');
            $pathToFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$poll->getFolder();

            $files = $request->files->all('itemImage');
            $names = $request->get('itemName');
            for($i =0;$i < count($files['itemImage']); $i++){
                $name = $names[$i];
                $file = $files['itemImage'][$i];

                $filename = md5(uniqid()).'.'.$file->guessExtension();
                $file->move($pathToFolder,$filename);

                $item = new Item();
                $item->setImage($config['media_folder'].'/'.$poll->getFolder().'/'.$filename);
                $item->setName($name);
                $item->setPoll($poll);
                $em->persist($item);
            }

            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_poll_list'));
        }

        return $this->render('ATMPollBundle:Poll:manage_items.html.twig',array(
            'poll' => $poll
        ));
    }

    /**
     * @Route("/edit/item/{itemId}", name="atm_poll_edit_item", options={"expose"=true})
     */
    function editPollItemAction($itemId){
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('ATMPollBundle:Item')->findOneById($itemId);

        $request = $this->get('request_stack')->getCurrentRequest();

        $newImage = $request->files->get('itemImage');
        if(!is_null($newImage)){
            $rootDir = $this->get('kernel')->getRootDir().'/../web/';
            $oldImage = $item->getImage();
            @unlink($rootDir.$oldImage);
            $path = dirname($oldImage);
            $filename = md5(uniqid()).'.'.$newImage->guessExtension();
            $newImage->move($rootDir.$path,$filename);
            $item->setImage($path.'/'.$filename);
        }

        $name = $request->get('itemName');
        if(!is_null($name)){
            $item->setName($name);
        }

        $em->persist($item);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_poll_add_item',array('pollId'=> $item->getPoll()->getId())));
    }


    /**
     * @Route("/delete/item/{itemId}", name="atm_poll_delete_item", options={"expose"=true})
     */
    public function deletePollItemAction($itemId){
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('ATMPollBundle:Item')->findOneById($itemId);
        $pollId = $item->getPoll()->getId();

        @unlink($rootDir = $this->get('kernel')->getRootDir().'/../web/'.$item->getImage());

        $em->remove($item);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_poll_add_item',array('pollId'=> $pollId)));
    }


    /**
     * @Route("/show/poll", name="atm_poll_show")
     */
    public function showPollAction(){
        $em = $this->getDoctrine()->getManager();

        $currentDate = new \DateTime();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('p')
            ->from('ATMPollBundle:Poll','p')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->lte('p.init_date',$qb->expr()->literal($currentDate->format('Y-m-d'))),
                    $qb->expr()->gte('p.end_date',$qb->expr()->literal($currentDate->format('Y-m-d')))
                )
            )
            ->orderBy('p.id','DESC');

        $poll = $qb->getQuery()->getResult();

        if(!empty($poll)){
            $poll = $poll[0];
            $pollId = $poll->getId();
            $items = $this->get(SearchVotes::class)->getItemsByVotes($pollId);
            $totalPollVotes = $this->get(SearchVotes::class)->search(array('poll_id'=>$pollId,'count'=>true));

            return $this->render('ATMPollBundle:Poll:show_poll.html.twig',array(
                'poll' => $poll,
                'items' => $items['results'],
                'totalPollVotes' => $totalPollVotes['count']
            ));
        }else{
            return $this->render('ATMPollBundle:Poll:poll_not_found.html.twig');
        }
    }

    /**
     * @Route("/vote/{itemId}/{pollId}", name="atm_poll_item_vote", options={"expose"=true})
     */
    public function voteAction($itemId,$pollId){
        $em = $this->getDoctrine()->getManager();
        $poll = $em->getRepository('ATMPollBundle:Poll')->findOneById($pollId);

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $voteCount = $this->get(SearchVotes::class)->search(array(
            'poll_id' => $pollId,
            'user_id' => $user->getId(),
            'count' => true
        ));

        $userNumVotes = $voteCount['count'];

        if($userNumVotes < $poll->getMaxNumberVotes() ){
            $voteCount = $this->get(SearchVotes::class)->search(array(
                'poll_id' => $pollId,
                'user_id' => $user->getId(),
                'item_id' => $itemId,
                'count' => true
            ));

            if($voteCount['count'] == 0){
                $vote = new Vote();
                $vote->setUserId($user->getId());
                $vote->setItemId($itemId);
                $vote->setPollId($pollId);

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($vote);
                $dm->flush();

                $totalPollVotes = $this->get(SearchVotes::class)->search(array('poll_id'=>$pollId,'count'=>true));
                $items = $this->get(SearchVotes::class)->getItemsByVotes($pollId);

                $arrVotes = array();
                foreach($items['results'] as $item){
                    $itemVotes =  $this->get(SearchVotes::class)->search(array('item_id' => $item['id'],'count' => true));
                    $arrVotes[] = array(
                        'item_id' => $item['id'],
                        'votes' => number_format(($itemVotes['count'] * 100) / $totalPollVotes['count'])
                    );

                }

                return new JsonResponse(array(
                    'code' => 'ok',
                    'msg' => 'You voted successfully.',
                    'votes' => $arrVotes
                ));
            }

            return new JsonResponse(array(
                'code' => 'ko',
                'msg' => 'You already voted in this poll.'
            ));

        }else{
            return new JsonResponse(array(
                'code' => 'ko',
                'msg' => 'You already voted in this poll.'
            ));
        }
    }

    /**
     * @Route("/users/voted/{pollId}/{page}", name="atm_poll_users_voted", defaults={"page":1})
     */
    public function usersVotedAction($pollId,$page){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_poll_config');
        $votes = $this->get(SearchVotes::class)->search(array('poll_id'=>$pollId));

        $userIds = array();
        $voteDate = array();
        foreach($votes['results'] as $vote){
            $userIds[] = $vote->getUserId();
            $voteDate[$vote->getUserId()] = $vote->getCreationDate();
        }

        $pagination = $users = null;
        if(count($userIds) > 0){

            $pagination = $this->get('knp_paginator')->paginate(
                $userIds,
                $page,
                20
            );

            $qbUsers = $em->createQueryBuilder();
            $qbUsers
                ->select('u')
                ->addSelect('FIELD(u.id, '.implode(',', array_reverse($pagination->getItems())).') AS HIDDEN sorting')
                ->from($config['user'],'u')
                ->where($qbUsers->expr()->in('u.id',$pagination->getItems()))
                ->orderBy('sorting');

            $users = $qbUsers->getQuery()->getArrayResult();
        }



        return $this->render('ATMPollBundle:Poll:usersVoted.html.twig',array(
            'users' => $users,
            'pagination' => $pagination,
            'poll' => $em->getRepository('ATMPollBundle:Poll')->findOneById($pollId),
            'voteDate' => $voteDate
        ));
    }
}
