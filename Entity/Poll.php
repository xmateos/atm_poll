<?php

namespace ATM\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ATM\PollBundle\Helpers\Canonicalizer;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ATM\PollBundle\Repository\PollRepository")
 * @ORM\Table(name="atm_poll")
 */
class Poll{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="canonical", type="string", length=255, nullable=false)
     */
    private $canonical;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(name="init_date", type="datetime", nullable=false)
     */
    private $init_date;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $end_date;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="poll", cascade={"remove"})
     */
    private $items;

    /**
     * @ORM\Column(name="folder",  type="string", length=255, nullable=false)
     */
    private $folder;

    /**
     * @ORM\Column(name="max_number_votes", type="integer", nullable=false)
     */
    private $max_number_votes;

    public function __construct()
    {
        $this->creation_date = new DateTime();
        $this->items = new ArrayCollection();
        $this->max_number_votes = 1;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        $this->canonical = Canonicalizer::canonicalize($name);
    }

    public function getCanonical()
    {
        return $this->canonical;
    }

    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getInitDate()
    {
        return $this->init_date;
    }

    public function setInitDate($init_date)
    {
        $this->init_date = $init_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    public function getMaxNumberVotes()
    {
        return $this->max_number_votes;
    }

    public function setMaxNumberVotes($max_number_votes)
    {
        $this->max_number_votes = $max_number_votes;
    }

    public function getItems(){
        return $this->items;
    }

    public function addItem($item){
        $this->items->add($item);
    }

    public function removeItem($item){
        $this->items->removeElement($item);
    }

    public function getFolder()
    {
        return $this->folder;
    }

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }
}