<?php

namespace ATM\PollBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use ATM\PollBundle\Entity\Poll;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,array(
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Name',
                    'autocomplete' => 'off'
                )
            ))
            ->add('init_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Init Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('end_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'End Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('max_number_votes',NumberType::class,array('required' => false,'attr'=>array('value'=>1)))
            ->add('description',TextareaType::class,array('required'=>false,'attr'=> array('placeholder'=>'Description')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Poll::class,
        ));
    }

    public function getName()
    {
        return 'atmcompetition_bundle_product_type';
    }
}