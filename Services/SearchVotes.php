<?php

namespace ATM\PollBundle\Services;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use ATM\PollBundle\Document\Vote;
use \MongoDate;

class SearchVotes{

    private $dm;
    private $em;
    private $paginator;


    public function __construct(DocumentManager $dm, EntityManagerInterface $em ,PaginatorInterface $paginator)
    {
        $this->dm = $dm;
        $this->em = $em;
        $this->paginator = $paginator;
    }


    public function search($options){
        $defaultOptions = array(
            'poll_id' => null,
            'user_id' => null,
            'item_id' => null,
            'creation_date' => null,
            'ids' => null,
            'sorting_field' => null,
            'date_limit' => array(
                'min' => null,
                'max' => null
            ),
            'limit' => null,
            'hydrate' => true,
            'page' => 1,
            'max_results' => null,
            'pagination' => null,
            'count' => null
        );

        $options = array_merge($defaultOptions, $options);

        $qb = $this->dm->createQueryBuilder(Vote::class);
        $qb->select('id','poll_id','user_id','item_id','creation_date');


        if(!is_null($options['poll_id'])){
            $qb->field('poll_id')->equals((int)$options['poll_id']);
        }

        if(!is_null($options['user_id'])){
            $qb->field('user_id')->equals((int)$options['user_id']);
        }

        if(!is_null($options['item_id'])){
            $qb->field('item_id')->equals((int)$options['item_id']);
        }

        if(!is_null($options['date_limit']['min'])){
            $min_date = $options['date_limit']['min'];
            $qb->field('creation_date')->gte(new MongoDate($min_date->format('U'),$min_date->format('u')));
        }

        if(!is_null($options['date_limit']['max'])){
            $max_date = $options['date_limit']['max'];
            $qb->field('creation_date')->lte(new MongoDate($max_date->format('U'),$max_date->format('u')));
        }

        if(!is_null($options['limit'])){
            $qb->limit($options['limit']);
        }

        if(is_null($options['pagination'] && $options['max_results'])){
            $qb->limit($options['max_results']);
        }

        $qb->sort('creation_date','ASC');
        $query = $qb->hydrate($options['hydrate'])->getQuery();


        if(!is_null($options['count'])){
            return array('count'=>$query->execute()->count());
        }else{
            $pagination = null;
            if(!is_null($options['pagination'])){
                $pagination = $this->paginator->paginate(
                    $query->execute()->toArray(),
                    $options['page'],
                    $options['max_results']
                );
                $votes = $pagination->getItems();
            }else {
                $votes = $query->execute();
            }

            return array(
                'results' => $votes,
                'pagination' => $pagination
            );
        }
    }

    public function getItemsByVotes($pollId){

        $pollVotes = $this->search(array('poll_id' => $pollId));
        $qb = $this->em->createQueryBuilder();

        if(count($pollVotes['results']) > 0){
            $itemsVotes = array();
            foreach($pollVotes['results'] as $vote){
                if(!isset($itemsVotes[$vote->getItemId()])){
                    $itemsVotes[$vote->getItemId()] = 0;
                }
                $itemsVotes[$vote->getItemId()]++;
            }

            arsort($itemsVotes);

            $itemsIds = array_keys($itemsVotes);

            $qb
                ->select('i')
                ->addSelect('FIELD(i.id, '.implode(',', array_reverse($itemsIds)).') AS HIDDEN sorting')
                ->from('ATMPollBundle:Item','i')
                ->join('i.poll','p','WITH',$qb->expr()->eq('p.id',$pollId))
                ->orderBy('i.id','ASC');

            return array('results'=> $qb->getQuery()->getArrayResult());
        }else{
            $qb
                ->select('i')
                ->from('ATMPollBundle:Item','i')
                ->join('i.poll','p','WITH',$qb->expr()->eq('p.id',$pollId))
                ->orderBy('i.name','ASC');
            return array('results'=> $qb->getQuery()->getArrayResult());
        }


    }
}