<?php

namespace ATM\PollBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \DateTime;

/**
 * @ODM\Document
 */
class Vote{

    /**
     * @ODM\Id(strategy="auto")
     */
    private $id;

    /**
     * @ODM\Field(type="integer")
     */
    private $user_id;

    /**
     * @ODM\Field(type="integer")
     */
    private $poll_id;

    /**
     * @ODM\Field(type="integer")
     */
    private $item_id;

    /**
     * @ODM\Field(type="date")
     */
    private $creation_date;

    public function __construct()
    {
        $this->creation_date = new DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function getPollId()
    {
        return $this->poll_id;
    }

    public function setPollId($poll_id)
    {
        $this->poll_id = $poll_id;
    }

    public function getItemId()
    {
        return $this->item_id;
    }

    public function setItemId($item_id)
    {
        $this->item_id = $item_id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }
}