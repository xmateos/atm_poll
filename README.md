# Installation #

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/pollbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\PollBundle\ATMPollBundle(),
    ];
}
```

# Configuration sample #
``` yml
# app/config/config.yml
  
atm_poll:
    media_folder: path to media folder
```

# Routing #
Append to the main routing file:
```yml
# app/config/routing.yml

atm_poll:
    resource: "@ATMPollBundle/Controller/PollController.php"
    type:     annotation
    prefix:   /members
```
# Services #
There is only 1 service that search votes and retrieve the items in a poll sorted by it's votes. These
are the functions:
```php
    public function search($options){
            $defaultOptions = array(
                'poll_id' => null,
                'user_id' => null,
                'item_id' => null,
                'creation_date' => null,
                'ids' => null,
                'sorting_field' => null,
                'date_limit' => array(
                    'min' => null,
                    'max' => null
                ),
                'limit' => null,
                'hydrate' => true,
                'page' => 1,
                'max_results' => null,
                'pagination' => null,
                'count' => null
            );
    }
```
If count is true it will return an array with the following structure:
```php
    array(
        'count' => $totalVotes
    );
```

```php
     public function getItemsByVotes($pollId){}
```

# Show a poll #
You can show the poll in you site by using the following call in you twig template:
```twig
    {{ render(controller('ATMPollBundle:Poll:showPoll',{
        'pollId' :  pollId
    })) }}
```
You can also use the following route to show the poll in its own page:
```twig
    {{ path('atm_poll_show',{ 'pollId':pollId }) }}
```

The view that returns it's in this path:

ATMPollBundle:Poll:show_poll.html.twig

And the variables that are passed to that view are the following:

    - poll -> poll object
    - items -> items in that poll sorted by number of votes
    - totalPollVotes -> total amount of votes of this poll

There is an extra feature to show confetti every time that a user votes successfully, these are
the steps to use it:

    - Include this script in your page:
        <script src="{{ asset('bundles/atmpoll/plugins/confetti/confetti.js') }}"></script>
    - Create a <canvas id="canvas"></canvas> somewhere in you page with these styles:
        canvas {display: block;position: fixed;z-index: 1;pointer-events: none;top:0;left:0}
    - Create these 2 buttons:
        <button id="confetti_start" style="display: none"></button>
        <button id="confetti_stop" style="display: none"></button>
    - Show and hide the confetti with this code:
        $('#confetti_start').click();
        setTimeout(function(){
            $('#confetti_stop').click();
        }, 2000);